Source: python-pyo
Section: python
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders: Tiago Bortoletto Vaz <tiago@debian.org>
Build-Depends: debhelper (>= 10), portaudio19-dev, libportmidi-dev, liblo-dev, libsndfile1-dev, libjack-jackd2-dev, dh-python, python3-all-dev, python3-setuptools
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/multimedia-team/python-pyo
Vcs-Git: https://salsa.debian.org/multimedia-team/python-pyo.git
Homepage: http://ajaxsoundstudio.com/software/pyo/

Package: python3-pyo
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, ${python3:Depends}
Recommends: python3-tk, jackd2
Description: Python3 module written in C to help digital signal processing script creation
 pyo is a Python module containing classes for a wide variety of audio signal
 processing types. With pyo, user will be able to include signal processing
 chains directly in Python scripts or projects, and to manipulate them in real
 time through the interpreter. Tools in pyo module offer primitives, like
 mathematical operations on audio signal, basic signal processing (filters,
 delays, synthesis generators, etc.), but also complex algorithms to create
 sound granulation and others creative audio manipulations.
 .
 pyo supports OSC protocol (Open Sound Control), to ease communications between
 softwares, and MIDI protocol, for generating sound events and controlling
 process parameters.
 .
 pyo allows creation of sophisticated signal processing chains with all the
 benefits of a mature, and wildly used, general programming language.
 .
 This package installs the library for Python 3.
